package utils;

import model.phone.Phone;

public class PhoneUtils {

    private PhoneUtils() {
    }

    public static Phone updatePhone(Phone oldPhone, Phone newPhone) {

        if (oldPhone == null
                && newPhone == null) {
            return null;
        }

        if (oldPhone == null) {
            return newPhone;
        }
        if (newPhone == null) {
            return oldPhone;
        }

        if (!oldPhone.getId().equals(newPhone.getId())) {
            return oldPhone;
        }

        if (newPhone.getAdditionalFeatures() != null) {
            oldPhone.setAdditionalFeatures(newPhone.getAdditionalFeatures());
        }

        if (newPhone.getAge() != 0) {
            oldPhone.setAge(newPhone.getAge());
        }

        if (newPhone.getAndroid() != null) {
            oldPhone.setAndroid(newPhone.getAndroid());
        }

        // if (!newPhone.getAvailability().isEmpty()) {
        // oldPhone.setAvailability(newPhone.getAvailability());
        // }
        //
        // if (newPhone.getBattery() != null) {
        // oldPhone.setBattery(newPhone.getBattery());
        // }
        //
        // if (newPhone.getCarrier() != null) {
        // oldPhone.setCarrier(newPhone.getCarrier());
        // }
        //
        // if (newPhone.getCamera() != null) {
        // oldPhone.setCamera(newPhone.getCamera());
        // }
        //
        // if (newPhone.getConnectivity() != null) {
        // oldPhone.setConnectivity(newPhone.getConnectivity());
        // }
        //
        // if (newPhone.getDescription() != null) {
        // oldPhone.setDescription(newPhone.getDescription());
        // }
        //
        // if (newPhone.getDisplay() != null) {
        // oldPhone.setDisplay(newPhone.getDisplay());
        // }
        //
        // if (newPhone.getHardware() != null) {
        // oldPhone.setHardware(newPhone.getHardware());
        // }
        //
        // if (!newPhone.getImages().isEmpty()) {
        // oldPhone.setImages(newPhone.getImages());
        // }
        //
        // if (newPhone.getImageUrl() != null) {
        // oldPhone.setImageUrl(newPhone.getImageUrl());
        // }
        //
        // if (newPhone.getName() != null) {
        // oldPhone.setName(newPhone.getName());
        // }
        //
        // if (newPhone.getSizeAndWeight() != null) {
        // oldPhone.setSizeAndWeight(newPhone.getSizeAndWeight());
        // }
        //
        // if (newPhone.getSnippet() != null) {
        // oldPhone.setSnippet(newPhone.getSnippet());
        // }
        //
        // if (newPhone.getStorage() != null) {
        // oldPhone.setStorage(newPhone.getStorage());
        // }

        return oldPhone;
    }
}
