package utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class SessionFactoryUtil {
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    public static SessionFactory getSessionFactory() {
        Configuration configuration =
                new Configuration();
        configuration.configure();
        serviceRegistry =
                new ServiceRegistryBuilder().applySettings(configuration.getProperties())
                        .buildServiceRegistry();

        sessionFactory =
                configuration.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory(Class<?>... classes) {
        Configuration configuration =
                new Configuration();
        configuration.configure();
        serviceRegistry =
                new ServiceRegistryBuilder().applySettings(configuration.getProperties())
                        .buildServiceRegistry();

        sessionFactory =
                addClass(configuration, classes).buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory(String packageName, Class<?>... classes) {
        Configuration configuration =
                new Configuration();
        configuration.configure();
        serviceRegistry =
                new ServiceRegistryBuilder().applySettings(configuration.getProperties())
                        .buildServiceRegistry();

        sessionFactory =
                addClass(configuration, classes).addPackage(packageName).buildSessionFactory(
                        serviceRegistry);
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory(String[] resources) {
        Configuration configuration =
                new Configuration();
        configuration.configure();
        serviceRegistry =
                new ServiceRegistryBuilder().applySettings(configuration.getProperties())
                        .buildServiceRegistry();
        sessionFactory =
                addRessources(configuration, resources).buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

    private static Configuration addRessources(Configuration config, String... resources) {

        for (String string : resources) {
            config.addResource(string);
        }

        return config;

    }

    private static Configuration addClass(Configuration config, Class<?>... classes) {

        for (Class<?> clazz : classes) {
            config.addAnnotatedClass(clazz);
        }
        return config;
    }
}
