package model.phone;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Generated("org.jsonschema2pojo")
@Entity
public class Display {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String screenResolution;
    private String screenSize;
    private boolean touchScreen;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Phone phone;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id =
                id;
    }

    /**
     * Sets the specified value for id and returns this.
     */
    public Display withId(long id) {
        this.id =
                id;
        return this;
    }

    public String getScreenResolution() {
        return screenResolution;
    }

    public void setScreenResolution(String screenResolution) {
        this.screenResolution =
                screenResolution;
    }

    /**
     * Sets the specified value for screenResolution and returns this.
     */
    public Display withScreenResolution(String screenResolution) {
        this.screenResolution =
                screenResolution;
        return this;
    }

    public String getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(String screenSize) {
        this.screenSize =
                screenSize;
    }

    /**
     * Sets the specified value for screenSize and returns this.
     */
    public Display withScreenSize(String screenSize) {
        this.screenSize =
                screenSize;
        return this;
    }

    public boolean getTouchScreen() {
        return touchScreen;
    }

    public void setTouchScreen(boolean touchScreen) {
        this.touchScreen =
                touchScreen;
    }

    /**
     * Sets the specified value for touchScreen and returns this.
     */
    public Display withTouchScreen(boolean touchScreen) {
        this.touchScreen =
                touchScreen;
        return this;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone =
                phone;
    }

    /**
     * Sets the specified value for phone and returns this.
     */
    public Display withPhone(Phone phone) {
        this.phone =
                phone;
        return this;
    }
}
