package model.phone;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Generated("org.jsonschema2pojo")
@Entity
public class Connectivity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String bluetooth;
    private String cell;
    private boolean gps;
    private boolean infrared;
    private String wifi;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Phone phone;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id =
                id;
    }

    /**
     * Sets the specified value for id and returns this.
     */
    public Connectivity withId(long id) {
        this.id =
                id;
        return this;
    }

    public String getBluetooth() {
        return bluetooth;
    }

    public void setBluetooth(String bluetooth) {
        this.bluetooth =
                bluetooth;
    }

    /**
     * Sets the specified value for bluetooth and returns this.
     */
    public Connectivity withBluetooth(String bluetooth) {
        this.bluetooth =
                bluetooth;
        return this;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell =
                cell;
    }

    /**
     * Sets the specified value for cell and returns this.
     */
    public Connectivity withCell(String cell) {
        this.cell =
                cell;
        return this;
    }

    public boolean getGps() {
        return gps;
    }

    public void setGps(boolean gps) {
        this.gps =
                gps;
    }

    /**
     * Sets the specified value for gps and returns this.
     */
    public Connectivity withGps(boolean gps) {
        this.gps =
                gps;
        return this;
    }

    public boolean getInfrared() {
        return infrared;
    }

    public void setInfrared(boolean infrared) {
        this.infrared =
                infrared;
    }

    /**
     * Sets the specified value for infrared and returns this.
     */
    public Connectivity withInfrared(boolean infrared) {
        this.infrared =
                infrared;
        return this;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi =
                wifi;
    }

    /**
     * Sets the specified value for wifi and returns this.
     */
    public Connectivity withWifi(String wifi) {
        this.wifi =
                wifi;
        return this;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone =
                phone;
    }

    /**
     * Sets the specified value for phone and returns this.
     */
    public Connectivity withPhone(Phone phone) {
        this.phone =
                phone;
        return this;
    }
}
