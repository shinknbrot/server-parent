package model.phone;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Generated("org.jsonschema2pojo")
@Entity
// @Table(uniqueConstraints = @UniqueConstraint(columnNames = {
// "os", "ui" }))
public class Android implements Comparable<Android> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String os;
    private String ui;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Phone phone;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int compareTo(Android o) {
        return os.compareTo(o.getOs());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id =
                id;
    }

    /**
     * Sets the specified value for id and returns this.
     */
    public Android withId(long id) {
        this.id =
                id;
        return this;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os =
                os;
    }

    /**
     * Sets the specified value for os and returns this.
     */
    public Android withOs(String os) {
        this.os =
                os;
        return this;
    }

    public String getUi() {
        return ui;
    }

    public void setUi(String ui) {
        this.ui =
                ui;
    }

    /**
     * Sets the specified value for ui and returns this.
     */
    public Android withUi(String ui) {
        this.ui =
                ui;
        return this;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone =
                phone;
    }

    /**
     * Sets the specified value for phone and returns this.
     */
    public Android withPhone(Phone phone) {
        this.phone =
                phone;
        return this;
    }
}
