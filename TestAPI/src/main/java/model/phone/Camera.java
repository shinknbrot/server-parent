package model.phone;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "CAMERA")
public class Camera {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ElementCollection
    @CollectionTable(name = "CAMERA_FEATURES", joinColumns = @JoinColumn(name = "ID"))
    private List<String> features =
            new ArrayList<String>();

    @Column(name = "PRIMARY_CAMERA")
    private String primary;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Phone phone;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public long getId() {

        return id;

    }

    public void setId(long id) {
        this.id =
                id;
    }

    /**
     * Sets the specified value for id and returns this.
     */
    public Camera withId(long id) {
        this.id =
                id;
        return this;
    }

    public List<String> getFeatures() {

        return features;

    }

    public void setFeatures(List<String> features) {
        this.features =
                features;
    }

    /**
     * Sets the specified value for features and returns this.
     */
    public Camera withFeatures(List<String> features) {
        this.features =
                features;
        return this;
    }

    public String getPrimary() {

        return primary;

    }

    public void setPrimary(String primary) {
        this.primary =
                primary;
    }

    /**
     * Sets the specified value for primary and returns this.
     */
    public Camera withPrimary(String primary) {
        this.primary =
                primary;
        return this;
    }

    public Phone getPhone() {

        return phone;

    }

    public void setPhone(Phone phone) {
        this.phone =
                phone;
    }

    /**
     * Sets the specified value for phone and returns this.
     */
    public Camera withPhone(Phone phone) {
        this.phone =
                phone;
        return this;
    }
}
