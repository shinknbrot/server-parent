package model.phone.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class AbstractModelDao<ModelType, KeyType> {

    protected Map<KeyType, ModelType> cache =
            new HashMap<>();

    public abstract List<ModelType> getAllModels();

    public abstract ModelType getModelById(KeyType id);

    public abstract ModelType insertModel(ModelType model);

    public abstract ModelType updateModel(ModelType model);

    public abstract ModelType deleteModel(KeyType id);

}
