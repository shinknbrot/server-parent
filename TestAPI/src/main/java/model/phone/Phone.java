package model.phone;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
@Entity
public class Phone {

    @Id
    @Column(name = "ID")
    private String id;

    @ElementCollection
    @CollectionTable(name = "Phone_availability", joinColumns = @JoinColumn(name = "ID"))
    private List<String> availability =
            new ArrayList<String>();

    @ElementCollection
    @CollectionTable(name = "Phone_images", joinColumns = @JoinColumn(name = "ID"))
    private List<String> images =
            new ArrayList<String>();

    private long age;
    private String carrier;

    private String imageUrl;
    private String name;
    private String snippet;

    @Column(columnDefinition = "text")
    private String additionalFeatures;

    @Column(columnDefinition = "text")
    private String description;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "android")
    private Android android;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "battery")
    private Battery battery;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "camera")
    private Camera camera;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "connectivity")
    private Connectivity connectivity;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "display")
    private Display display;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "hardware")
    private Hardware hardware;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sizeAndWeight")
    private SizeAndWeight sizeAndWeight;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "storage")
    private Storage storage;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getId() {

        return id;

    }

    public void setId(String id) {
        this.id =
                id;
    }

    /**
     * Sets the specified value for id and returns this.
     */
    public Phone withId(String id) {
        this.id =
                id;
        return this;
    }

    public List<String> getAvailability() {

        return availability;

    }

    public void setAvailability(List<String> availability) {
        this.availability =
                availability;
    }

    /**
     * Sets the specified value for availability and returns this.
     */
    public Phone withAvailability(List<String> availability) {
        this.availability =
                availability;
        return this;
    }

    public List<String> getImages() {

        return images;

    }

    public void setImages(List<String> images) {
        this.images =
                images;
    }

    /**
     * Sets the specified value for images and returns this.
     */
    public Phone withImages(List<String> images) {
        this.images =
                images;
        return this;
    }

    public long getAge() {

        return age;

    }

    public void setAge(long age) {
        this.age =
                age;
    }

    /**
     * Sets the specified value for age and returns this.
     */
    public Phone withAge(long age) {
        this.age =
                age;
        return this;
    }

    public String getCarrier() {

        return carrier;

    }

    public void setCarrier(String carrier) {
        this.carrier =
                carrier;
    }

    /**
     * Sets the specified value for carrier and returns this.
     */
    public Phone withCarrier(String carrier) {
        this.carrier =
                carrier;
        return this;
    }

    public String getImageUrl() {

        return imageUrl;

    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl =
                imageUrl;
    }

    /**
     * Sets the specified value for imageUrl and returns this.
     */
    public Phone withImageUrl(String imageUrl) {
        this.imageUrl =
                imageUrl;
        return this;
    }

    public String getName() {

        return name;

    }

    public void setName(String name) {
        this.name =
                name;
    }

    /**
     * Sets the specified value for name and returns this.
     */
    public Phone withName(String name) {
        this.name =
                name;
        return this;
    }

    public String getSnippet() {

        return snippet;

    }

    public void setSnippet(String snippet) {
        this.snippet =
                snippet;
    }

    /**
     * Sets the specified value for snippet and returns this.
     */
    public Phone withSnippet(String snippet) {
        this.snippet =
                snippet;
        return this;
    }

    public String getAdditionalFeatures() {

        return additionalFeatures;

    }

    public void setAdditionalFeatures(String additionalFeatures) {
        this.additionalFeatures =
                additionalFeatures;
    }

    /**
     * Sets the specified value for additionalFeatures and returns this.
     */
    public Phone withAdditionalFeatures(String additionalFeatures) {
        this.additionalFeatures =
                additionalFeatures;
        return this;
    }

    public String getDescription() {

        return description;

    }

    public void setDescription(String description) {
        this.description =
                description;
    }

    /**
     * Sets the specified value for description and returns this.
     */
    public Phone withDescription(String description) {
        this.description =
                description;
        return this;
    }

    public Android getAndroid() {

        return android;

    }

    public void setAndroid(Android android) {
        if (this.android != null) {
            this.android.setPhone(null);
        }
        this.android =
                android;
        this.android.setPhone(this);
    }

    /**
     * Sets the specified value for android and returns this.
     */
    public Phone withAndroid(Android android) {
        setAndroid(android);
        return this;
    }

    public Battery getBattery() {

        return battery;

    }

    public void setBattery(Battery battery) {
        if (this.battery != null) {
            this.battery.setPhone(null);
        }
        this.battery =
                battery;
        this.battery.setPhone(this);
    }

    /**
     * Sets the specified value for battery and returns this.
     */
    public Phone withBattery(Battery battery) {
        setBattery(battery);
        return this;
    }

    public Camera getCamera() {

        return camera;

    }

    public void setCamera(Camera camera) {
        if (this.camera != null) {
            this.camera.setPhone(null);
        }
        this.camera =
                camera;
        this.camera.setPhone(this);
    }

    /**
     * Sets the specified value for camera and returns this.
     */
    public Phone withCamera(Camera camera) {
        setCamera(camera);
        return this;
    }

    public Connectivity getConnectivity() {

        return connectivity;

    }

    public void setConnectivity(Connectivity connectivity) {
        if (this.connectivity != null) {
            this.connectivity.setPhone(null);
        }
        this.connectivity =
                connectivity;

        this.connectivity.setPhone(this);
    }

    /**
     * Sets the specified value for connectivity and returns this.
     */
    public Phone withConnectivity(Connectivity connectivity) {
        setConnectivity(connectivity);
        return this;
    }

    public Display getDisplay() {

        return display;

    }

    public void setDisplay(Display display) {
        if (this.display != null) {
            this.display.setPhone(null);
        }
        this.display =
                display;

        this.display.setPhone(this);
    }

    /**
     * Sets the specified value for display and returns this.
     */
    public Phone withDisplay(Display display) {
        setDisplay(display);
        return this;
    }

    public Hardware getHardware() {

        return hardware;

    }

    public void setHardware(Hardware hardware) {
        if (this.hardware != null) {
            this.hardware.setPhone(null);
        }
        this.hardware =
                hardware;

        this.hardware.setPhone(this);
    }

    /**
     * Sets the specified value for hardware and returns this.
     */
    public Phone withHardware(Hardware hardware) {
        setHardware(hardware);
        return this;
    }

    public SizeAndWeight getSizeAndWeight() {

        return sizeAndWeight;

    }

    public void setSizeAndWeight(SizeAndWeight sizeAndWeight) {
        if (this.sizeAndWeight != null) {
            this.sizeAndWeight.setPhone(null);
        }
        this.sizeAndWeight =
                sizeAndWeight;

        this.sizeAndWeight.setPhone(this);
    }

    /**
     * Sets the specified value for sizeAndWeight and returns this.
     */
    public Phone withSizeAndWeight(SizeAndWeight sizeAndWeight) {
        setSizeAndWeight(sizeAndWeight);
        return this;
    }

    public Storage getStorage() {

        return storage;

    }

    public void setStorage(Storage storage) {
        if (this.storage != null) {
            this.storage.setPhone(null);
        }
        this.storage =
                storage;

        this.storage.setPhone(this);
    }

    /**
     * Sets the specified value for storage and returns this.
     */
    public Phone withStorage(Storage storage) {
        setStorage(storage);
        return this;
    }
}