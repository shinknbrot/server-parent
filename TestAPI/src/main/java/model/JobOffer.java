package model;

public class JobOffer extends AbstractModel {

    private String title;
    private String description;
    private String enterpriseId;
    private String contactId;

    public JobOffer() {
        super(null);
    }

    public JobOffer(String id, String title, String description) {
        super(id);
        this.title =
                title;
        this.description =
                description;
    }

    public JobOffer(String id, String title, String description, String enterpriseId) {
        this(id, title, description);
        this.enterpriseId =
                enterpriseId;
    }

    public JobOffer(String id, String title, String description, String enterpriseId,
            String contactId) {
        this(id, title, description, enterpriseId);
        this.contactId =
                contactId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title =
                title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description =
                description;
    }

    public String getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(String enterpriseId) {
        this.enterpriseId =
                enterpriseId;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId =
                contactId;
    }

    @Override
    public String toString() {
        return "JobOffer [getId()="
                + getId() + "title=" + title + ", description=" + description + ", enterpriseId="
                + enterpriseId + ", contactId=" + contactId + "]";
    }

}
