package main.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import model.phone.Phone;
import model.phone.dao.PhoneDao;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dao.PhoneDaoImpl;

@RestController
@RequestMapping("/phones")
public class PhoneController extends AbstractController<Phone> {

    private static final Logger LOGGER =
            Logger.getLogger(PhoneController.class);

    private PhoneDao phoneDao =
            new PhoneDaoImpl();

    public PhoneController() {
        for (Phone phone : phoneDao.getAllModels()) {
            modelDB.put(phone.getId(), phone);
        }
    }

    @Override
    public Collection<Phone> getAllModels() {
        return super.getAllModels();
    }

    @Override
    public Phone createModel(@RequestBody final Phone model) {
        LOGGER.info("#createModel "
                + model);
        if (!modelDB.containsKey(model.getId())) {
            modelDB.put(model.getId(), model);
            phoneDao.insertModel(model);
        }
        return model;

    }

    @Override
    public Phone updateModel(@PathVariable final String id, @RequestBody final Phone model) {
        LOGGER.info("#updateModel "
                + model);
        return phoneDao.updateModel(model);
    }

    @Override
    public List<Phone> updateModel(@RequestBody final Phone[] modelArr) {

        List<Phone> liste =
                new ArrayList<>();
        for (Phone model : modelArr) {
            liste.add(updateModel(model.getId(), model));
        }
        return liste;
    }

    @Override
    public Phone updateModel(@RequestBody final Phone model) {

        return updateModel(model.getId(), model);
    }

    @Override
    public Phone deleteModel(@PathVariable final Phone model) {
        phoneDao.deleteModel(model.getId());
        return deleteModel(model.getId());
    }
}
