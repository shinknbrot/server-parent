package test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import questionandanswers.Question;
import utils.SessionFactoryUtil;

public class StoreData {
    private static SessionFactory sessionFactory =
            SessionFactoryUtil.getSessionFactory("questionandanswers/Question.hbm.xml");

    public static void main(String[] args) {
        store();
        fetch();
    }

    private static void store() {

        Session session =
                sessionFactory.openSession();

        Transaction t =
                session.beginTransaction();

        ArrayList<String> list1 =
                new ArrayList<String>();
        list1.add("java is a programming language");
        list1.add("java is a platform");

        ArrayList<String> list2 =
                new ArrayList<String>();
        list2.add("Servlet is an Interface");
        list2.add("Servlet is an API");

        Question question1 =
                new Question();
        question1.setQname("What is Java?");
        question1.setAnswers(list1);

        Question question2 =
                new Question();
        question2.setQname("What is Servlet?");
        question2.setAnswers(list2);

        session.persist(question1);
        session.persist(question2);

        t.commit();
        session.close();
        System.out.println("success");
    }

    private static void fetch() {

        Session session =
                sessionFactory.openSession();

        Query query =
                session.createQuery("from Question");
        List<Question> list =
                query.list();

        Iterator<Question> itr =
                list.iterator();
        while (itr.hasNext()) {
            Question q =
                    itr.next();
            System.out.println("Question Name: "
                    + q.getQname());

            // printing answers
            List<String> list2 =
                    q.getAnswers();
            Iterator<String> itr2 =
                    list2.iterator();
            while (itr2.hasNext()) {
                System.out.println(itr2.next());
            }

        }
        session.close();
        System.out.println("success");

    }
}
